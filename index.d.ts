import { QueryResult, QueryResultRow, PoolConfig } from "pg";

declare class PgHelper {
    static instance(): Promise<PgHelper>;

    static init(config: PoolConfig & { poolSize: number }): void;

    private constructor();

    query<
        R extends QueryResultRow = any, 
        I extends any[] = any[]
    >(sql: string, params?: I): Promise<QueryResult<R>>;

    queryCursor<
        R extends QueryResultRow = any, 
        I extends any[] = any[]
    >(sql: string, params?: I):Promise<R[]>;

    static queryAlone<
        R extends QueryResultRow = any, 
        I extends any[] = any[]
    >(sql: string, params?: I): Promise<QueryResult<R>>;

    static queryCursorAlone<
        R extends QueryResultRow = any, 
        I extends any[] = any[]
    >(sql: string, params?: I):Promise<R[]>;

    begin(): PgHelper;

    end(): Promise<void>;

    commit(): Promise<void>;

    rollback(): Promise<void>;

    close(end: boolean): Promise<void | null>;
}

export = PgHelper;