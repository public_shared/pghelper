# pghelper

pghelper - это обертка над [pg](https://www.npmjs.com/package/pg) для инкапсуляции некоторой логики работы с базой данных и   
более простого и удобного взаимодействия с ней.

#### зависимости:
[pg](https://www.npmjs.com/package/pg) версия 8.7.3

## установка

Вы можете воспользоваться npm для установки этого пакета из репозитория gitlab:  
``` npm install https://gitlab.com/public_shared/pghelper.git ```  

## начало работы  

Импортируйте *pghelper* в корневом файле приложения.
Вызовите статический метод *init*, передав ему данные для подключения к БД.

```javascript
const PgHelper = require('pghelper');


PgHelper.init({
    connectionString: "postgresql://user:password@localhost:5432/db_name",
    user: "user",
    password: "password",
    host: "localhost",
    port: "5432",
    database: "db_name",
    poolSize: 2
})
 ```  

 Готово! Теперь вы можете использовать *pghepler* для запросов к вашей БД.  
  
## Запросы к БД

Для запросов к базе данных, запуска и закрытия транзакций используются методы
класса PgHelper

### одиночные запросы

* для простых одиночных запросов используется статический метод *queryAlone*.  
в результате вызова метода вернется объект вида *{ rows: [] }*  
где rows - массив с набором строк, которые вернул запрос. в случае осутствия строк будет пустой массив.

* для одиночных запросов, которые возвращают *refcursor* используется метод *queryCursorAlone*,  
который вернет массив с набором строк, которые вернул запрос.

Примеры:

```javascript

const PgHelper = require('pghelper')


( async() => {

    // запрос без параметров
    const result1 = await PgHelper.queryAlone('SELECT * FROM table;');
    console.log(result1.rows)

    // запрос с параметрами
    const result2 = await PgHelper.queryAlone('SELECT * FROM table WHERE table_id=$1;', [ 1 ]);
    console.log(result1.rows);

    // запрос к процедуре, которая возвращает курсор
    const resultRows1 = await PgHelper.queryCursorAlone('SELECT returns_cursor_procedure()');

    // запрос к процедуре с параметрами, которая возвращает курсор 
    const resultRows2 = await PgHelper.queryCursorAlone('SELECT returns_cursor_procedure(p_parameter => $1)', [ 1 ]);


})()
```

### транзакции

* Для открытия транзакции нужно с помощью статического метода *instance*   
получить экземпляр класса *PgHelper* ( он автоматически получит connection из пула)
и у него вызвать метод *begin* (запустит транзакцию).  

* Внутри тразакции для простых запросов и запросов к курсорам используются методы *query* и *queryCursor* соответственно.

* Для отката транзакции используется метод *rollback* 

* Для того, чтобы зафиксировать транзакцию используется метод *end* или *commit* (они эквивалентны);

* Для того, чтобы вернуть connection обратно в пул, используется метод *close(end: boolean)*.   
Если ему передать true, то он зафиксирует текущую транзакцию.


Примеры:

```javascript

const PgHelper = require('pghelper');


( async() => {

    let pginstance = null;

    try {
        /* Вернет экземпляр PgHelper и начнет транзакцию */
        pginstance = await( await PgHelper.instance() ).begin();

        //!!! методы query и queryCursor не статичные, а принадлежат экземпляру класса PgHelper.

        // запрос с параметрами
        const result2 = await pginstance.query('SELECT * FROM table WHERE table_id=$1;', [ 1 ]);
        console.log(result1.rows);

        // запрос к процедуре, которая возвращает курсор
        const resultRows1 = await pginstance.queryCursor('SELECT returns_cursor_procedure()');

        // запрос к процедуре с параметрами, которая возвращает курсор 
        const resultRows2 = await pginstance.queryCursor('SELECT returns_cursor_procedure(p_parameter => $1)', [ 1 ]);
    } catch(err) {
        // в случае ошибки откатываем транзакцию
        pginstance && await pginstance.rollback();
    } finally {
        // возвращаем соединение в пул
        pginstance && await pginstance.close(true);
    }
})()
```

